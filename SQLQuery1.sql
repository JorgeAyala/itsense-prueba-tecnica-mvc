create database dbInventario

USE dbInventario

-- Creamos la tabla principal de productos
create table PRODUCTOS (
	id_producto int identity primary key,
	nombre varchar(255),
	tipo varchar(255),
	estado bit,
	cantidad int
)

-- Sp necesario para a�adir un producto
CREATE PROCEDURE SP_a�adir_producto
    @id_producto INT,
    @cantidad_agregar INT
AS
BEGIN
    SET NOCOUNT ON;

    UPDATE PRODUCTOS
    SET cantidad = cantidad + @cantidad_agregar
    WHERE id_producto = @id_producto;

    IF @@ROWCOUNT = 0
    BEGIN
        PRINT 'Producto no encontrado.';
    END
    ELSE
    BEGIN
        PRINT 'Cantidad a�adida exitosamente.';
    END
END;

-- Sp necesario para realizar salida de producto
CREATE PROCEDURE SP_retirar_producto
    @id_producto INT,
    @cantidad_sacar INT
AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @cantidad_actual INT;

    SELECT @cantidad_actual = cantidad
    FROM PRODUCTOS
    WHERE id_producto = @id_producto;

    IF @cantidad_actual IS NULL
    BEGIN
        PRINT 'Producto no encontrado.';
    END
    ELSE IF @cantidad_actual < @cantidad_sacar
    BEGIN
        PRINT 'Cantidad insuficiente en el inventario.';
    END
    ELSE
    BEGIN
        UPDATE PRODUCTOS
        SET cantidad = cantidad - @cantidad_sacar
        WHERE id_producto = @id_producto;

        PRINT 'Cantidad retirada exitosamente.';
    END
END;

-- Sp necesario para filtrar productos de acuerdo al tipo
CREATE PROCEDURE SP_listar_producto_tipo
    @tipo VARCHAR(255)
AS
BEGIN
    SET NOCOUNT ON;

    SELECT id_producto, nombre, tipo, estado, cantidad
    FROM PRODUCTOS
    WHERE tipo = @tipo;
END;

-- Sp necesario para filtrar los productos de a cuerdo al estado (defectuoso 1, no defectuoso 0)
CREATE PROCEDURE SP_listar_producto_estado
    @estado BIT
AS
BEGIN
    SET NOCOUNT ON;

    SELECT id_producto, nombre, tipo, estado, cantidad
    FROM PRODUCTOS
    WHERE estado = @estado;
END;

-- Sp necesario para cambiar el estado de un producto
CREATE PROCEDURE SP_cambiar_estado_producto
    @id_producto INT,
    @estado BIT
AS
BEGIN
    SET NOCOUNT ON;

    UPDATE PRODUCTOS
    SET estado = @estado
    WHERE id_producto = @id_producto;

    IF @@ROWCOUNT = 0
    BEGIN
        PRINT 'Producto no encontrado.';
    END
    ELSE
    BEGIN
        PRINT 'Estado actualizado exitosamente.';
    END
END;

-- Sp necesario para crear un nuevo producto
CREATE PROCEDURE SP_crear_producto
    @nombre VARCHAR(255),
    @tipo VARCHAR(255),
    @estado BIT,
    @cantidad INT
AS
BEGIN
    INSERT INTO PRODUCTOS (nombre, tipo, estado, cantidad)
    VALUES (@nombre, @tipo, @estado, @cantidad);
END;

-------------------------------------------------------------

-- Insertar el producto "Laptop"
INSERT INTO PRODUCTOS (nombre, tipo, estado, cantidad)
VALUES ('Laptop', 'Electr�nicos', 0, 10);

-- Insertar el producto "Mouse"
INSERT INTO PRODUCTOS (nombre, tipo, estado, cantidad)
VALUES ('Mouse', 'Perif�ricos', 0, 25);

-- Insertar el producto "Teclado"
INSERT INTO PRODUCTOS (nombre, tipo, estado, cantidad)
VALUES ('Teclado', 'Perif�ricos', 0, 15);

-- Insertar el producto "Monitor"
INSERT INTO PRODUCTOS (nombre, tipo, estado, cantidad)
VALUES ('Monitor', 'Electr�nicos', 0, 5);

-- Insertar el producto "Cable HDMI"
INSERT INTO PRODUCTOS (nombre, tipo, estado, cantidad)
VALUES ('Cable HDMI', 'Accesorios', 0, 50);

select * from PRODUCTOS

truncate table PRODUCTOS

